/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import {By} from "protractor/built/index";

describe('AppComponent', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
    });
    TestBed.compileComponents();
  });

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it('should render title in a h2 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h2').textContent).toContain('Subscription & billing');
  }));

  it('should render service', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelectorAll('strong')[1].textContent).toContain('Premium account for €2.99 per/month');
  }));

  /**
   * Since subscribe and unsubscribe require correct state to start with we reset it if needed.
   */
  let resetState = function(){
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    if(compiled.querySelector('#nextPayment').textContent.length == 10){
      compiled.querySelector('#subscribeToggle').click();
      fixture.detectChanges();
    }
  };

  it('should subscribe', async(() => {
    resetState();
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    compiled.querySelector('#subscribeToggle').click();
    fixture.detectChanges();
    expect(compiled.querySelector('#nextPayment').textContent.length).toBe(10);
  }));

  it('should unsubscribe', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    compiled.querySelector('#subscribeToggle').click();
    fixture.detectChanges();
    expect(compiled.querySelector('#nextPayment').textContent).toBe("-");
  }));

});
