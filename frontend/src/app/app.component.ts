import { Component } from '@angular/core';
import * as $ from 'jquery';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [],
})
export class AppComponent {
  title = 'Subscription & billing';
  customer: any[];
  labelSubscribe = 'Subscribe';
  labelUnsubscribe = 'Cancel subscription';

  /**
   * GETs synchronously data from url
   * @param url - url to request
   * @returns {string} - response from server
   */
   getURL(url){
    return $.ajax({
      type: "GET",
      url: url,
      cache: false,
      async: false
    }).responseText;
  }

  /**
   * POSTs synchronously JSON data to specified url
   * @param url - Url to post data
   * @param jsonData - JSON data to post
   * @returns {string} - response from server
   */
  postURL(url, jsonData){
    return $.ajax({
      type: "POST",
      url: url,
      data: JSON.stringify(jsonData),
      dataType: "json",
      contentType: 'application/json',
      cache: false,
      async: false
    }).responseText;
  }

  /**
   * Hides next payment when customer is not subscribed
   * @param json - Customer object
   * @returns {any} - modified customer object
   */
  processResponse(json) {
    if(!json.isPremium){
      json.nextPayment = "-";
    }
    return json;
  }

  /**
   * Toggles subscription for given user
   */
  toggleSubscription(){
    let response = this.postURL("http://localhost:8080/api/customer/543210/",{"isPremium": !this.customer["isPremium"]});
    let responseJSON = JSON.parse(response);
    responseJSON =  this.processResponse(responseJSON);
    this.customer =  responseJSON;
    $("#nextPayment").html(responseJSON.nextPayment);
    let toggle = $("#subscribeToggle");
    if(responseJSON.isPremium){
      toggle.html(this.labelUnsubscribe);
    } else {
      toggle.html(this.labelSubscribe);
    }
  }

  constructor() {
    // I know that the angular ways is http.get("...") but I gave up after spending entire day trying to get it work
    let response = this.getURL("http://localhost:8080/api/customer/543210");
    let responseJSON = JSON.parse(response);
    this.customer = this.processResponse(responseJSON);
  }

}
