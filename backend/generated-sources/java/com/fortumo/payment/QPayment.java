package com.fortumo.payment;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QPayment is a Querydsl query type for Payment
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QPayment extends EntityPathBase<Payment> {

    private static final long serialVersionUID = -609802545L;

    public static final QPayment payment = new QPayment("payment");

    public final NumberPath<Double> cost = createNumber("cost", Double.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final DateTimePath<java.util.Date> paymentDate = createDateTime("paymentDate", java.util.Date.class);

    public final DateTimePath<java.util.Date> periodEnd = createDateTime("periodEnd", java.util.Date.class);

    public final DateTimePath<java.util.Date> periodStart = createDateTime("periodStart", java.util.Date.class);

    public QPayment(String variable) {
        super(Payment.class, forVariable(variable));
    }

    public QPayment(Path<? extends Payment> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPayment(PathMetadata<?> metadata) {
        super(Payment.class, metadata);
    }

}

