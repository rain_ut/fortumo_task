package com.fortumo.customer;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QCustomer is a Querydsl query type for Customer
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QCustomer extends EntityPathBase<Customer> {

    private static final long serialVersionUID = -189800289L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QCustomer customer = new QCustomer("customer");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final BooleanPath isPremium = createBoolean("isPremium");

    public final ListPath<com.fortumo.payment.Payment, com.fortumo.payment.QPayment> payments = this.<com.fortumo.payment.Payment, com.fortumo.payment.QPayment>createList("payments", com.fortumo.payment.Payment.class, com.fortumo.payment.QPayment.class, PathInits.DIRECT2);

    public final NumberPath<Long> phone = createNumber("phone", Long.class);

    public final com.fortumo.service.QService service;

    public QCustomer(String variable) {
        this(Customer.class, forVariable(variable), INITS);
    }

    public QCustomer(Path<? extends Customer> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QCustomer(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QCustomer(PathMetadata<?> metadata, PathInits inits) {
        this(Customer.class, metadata, inits);
    }

    public QCustomer(Class<? extends Customer> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.service = inits.isInitialized("service") ? new com.fortumo.service.QService(forProperty("service")) : null;
    }

}

