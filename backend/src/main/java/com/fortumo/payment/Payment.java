package com.fortumo.payment;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;


/**
 * Payment entity
 */
@Getter
@Setter
@Entity
public class Payment {
    @Id
    Long id;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    Date paymentDate;
    @JsonFormat(pattern="yyyy-MM-dd")
    Date periodStart;
    @JsonFormat(pattern="yyyy-MM-dd")
    Date periodEnd;
    Double cost;

}
