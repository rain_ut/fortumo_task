package com.fortumo.customer;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Class for interacting with customer repository.
 */
@org.springframework.stereotype.Service
public class CustomerService {
    @Autowired
    CustomerRepository customerRepository;

    /**
     * Lists all customers
     * @return
     */
    public List<Customer> findAll(){
        return customerRepository.findAll();
    }

    /**
     * Finds customer by phone number
     * @param number customers phone number
     * @return CustomerDTO of found customer
     */
    public CustomerDTO findByPhone(Long number){
        Customer customer = customerRepository.findByPhone(number);
        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO = customerDTO.of(customer);
        return customerDTO;
    }

    /**
     * Subscribes or unsubscribes customer
     * @param phone customers phone
     * @param isPremium new premium status
     * @return Customer with updated data
     */
    public Customer updateIsPremium(Long phone, Boolean isPremium) {
        Customer customer = customerRepository.findByPhone(phone);
        customer.setIsPremium(isPremium);
        return customerRepository.save(customer);
    }
}
