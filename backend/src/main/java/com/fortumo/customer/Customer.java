package com.fortumo.customer;

import com.fortumo.payment.Payment;
import com.fortumo.service.Service;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

/**
 * Customer entity
 */
@Getter
@Setter
@Entity
public class Customer {
    @Id
    Long id;
    @OneToOne
    Service service;
    Long phone;
    Boolean isPremium;
    @OneToMany
    List<Payment> payments;

}
