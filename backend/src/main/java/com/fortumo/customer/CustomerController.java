package com.fortumo.customer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * A rest controller for accessing customer
 */
@RestController
public class CustomerController {
    private static final Logger LOG = LoggerFactory.getLogger(CustomerController.class);

    @Autowired
    CustomerService customerService;

    @CrossOrigin
    @RequestMapping("/api/customer/{phone:.+}")
    public CustomerDTO getServiceByName(@PathVariable("phone") Long phone) {
        LOG.info("Requested schema for {}", phone);
        return customerService.findByPhone(phone);
    }

    @CrossOrigin
    @RequestMapping(value="/api/customer/{phone:.+}", method = RequestMethod.POST)
    public CustomerDTO setPremium(@PathVariable("phone") Long phone, @RequestBody CustomerDTO customerDTO) {
        LOG.info("Requested schema for {}", phone);
        customerService.updateIsPremium(phone, customerDTO.isPremium);
        return customerService.findByPhone(phone);
    }

    @RequestMapping("/api/customers")
    public List<Customer> getSchemas() {
        return customerService.findAll();
    }

}
