package com.fortumo.customer;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fortumo.payment.Payment;
import com.fortumo.service.Service;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.time.DateUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Data Tranfer Object for customer
 * Used to add next payment date to Customer and to avoid customer id from leaking out to possible attackers
 */
@Getter
@Setter
public class CustomerDTO {
    Service service;
    Long phone;
    Boolean isPremium;
    List<Payment> payments;
    @JsonFormat(pattern="yyyy-MM-dd")
    Date nextPayment;

    /**
     * Creates CustomerDTO of Customer
     * @param customer
     * @return
     */
    public CustomerDTO of(Customer customer){
        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setService(customer.service);
        customerDTO.setPhone(customer.phone);
        customerDTO.setIsPremium(customer.isPremium);
        customerDTO.setPayments(customer.payments);
        Date newDate;
        if(customer.payments.size() > 0 && customer.isPremium) {
            Date maxDate = customer.payments.stream().map(Payment::getPaymentDate).max(Date::compareTo).get();
            newDate = DateUtils.addMonths(maxDate, 1);
        }else if(customer.payments.size() == 0 && customer.isPremium){
            newDate = DateUtils.addMonths(new Date(), 1);
        }else{
            newDate = null;
        }
        customerDTO.setNextPayment(newDate);
        return customerDTO;
    }

    public List<CustomerDTO> of(List<Customer> customers){
        List<CustomerDTO> customerDTOs = new ArrayList<>();
        return customerDTOs;
    }

}
