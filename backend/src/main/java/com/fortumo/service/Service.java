package com.fortumo.service;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/*
Service entity
 */
@Getter
@Setter
@Entity
public class Service {
    @Id
    Long id;
    Double price;
    String name;
    String provider;

}
