package com.fortumo.customer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fortumo.FortumoApplication;
import com.fortumo.customer.CustomerDTO;
import org.hamcrest.core.IsNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {FortumoApplication.class})
@WebAppConfiguration
@DirtiesContext
public class CustomerTest {

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;
    @Autowired
    @Qualifier("_halObjectMapper")
    ObjectMapper mapper;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }


    @Test
    public void getCustomerByPhone() throws Exception {
        mockMvc.perform(get("/api/customer/5357234")).andExpect(status().is(200));
    }

    @Test
    public void getCustomerNextPayment() throws Exception {
        mockMvc.perform(get("/api/customer/5357234")).andExpect(jsonPath("nextPayment").value("2017-01-01"));
    }

    @Test
    public void subscription() throws Exception {
        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setIsPremium(false);

        mockMvc.perform(
                post("/api/customer/5357234")
                        .content(mapper.writeValueAsString(customerDTO))
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(jsonPath("isPremium").value(false));

        mockMvc.perform(get("/api/customer/5357234")).andExpect(jsonPath("isPremium").value(false))
                .andExpect(jsonPath("nextPayment").value(IsNull.nullValue()));

        //restore previous state
        customerDTO.setIsPremium(true);
        mockMvc.perform(
                post("/api/customer/5357234")
                        .content(mapper.writeValueAsString(customerDTO))
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(jsonPath("isPremium").value(true));
    }

}