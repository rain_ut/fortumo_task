# Backend server
This is a backend server for subscription application

## Running
To run this application run:
```
mvn package
java -jar ./target/fortumo-1.0-SNAPSHOT.jar
```
If you use IDE run java/com.fortumo.FortumoApplication.

## Testing
To run tests you can run. 
```
mvn test
```
If you use IDE run tests/com.fortumo.customer.CustomerTests.

## Developing
This program is divided into packages by data entities which are customer, payment and service.

Each package contains entity and repository. In addition, some packages also contain a service for that entity. The purpose of service is to avoid accessing repository directly from the controller. The rest controller accesses repository via service.
In addition, there is Data Transfer Object (DTO) for Customer class. This is used to add next payment field to Customer class. Also to Customer ids from leaking out via frontend.
